// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require plugins/jquery.readySelector
//= require ckeditor/init
//= require bootstrap
//= require light-gallery
//= require owl.carousel.min
//= require instafeed.min
//= require turbolinks
//= require_tree .

$(document).on('turbolinks:load', function() {
  var today = new Date();
  console.log($('body').attr('class') + today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds());

  // on every page except home page
  if (!$('body.home.index').length > 0) {
    // const mqq = window.matchMedia( "(max-width: 767px)" );

    // if (!mqq.matches) {
    //   // window width is not less than 767px
    //   !function(t,e){"use strict";var r=function(t){try{var r=e.head||e.getElementsByTagName("head")[0],a=e.createElement("script");a.setAttribute("type","text/javascript"),a.setAttribute("src",t),r.appendChild(a)}catch(t){}};t.CollectId = "5a4bc166dfcbbab4aa7c78b2",r("https://s3.amazonaws.com/collectchat/launcher.js")}(window,document);
    // }; 
    !function(t,e){"use strict";var r=function(t){try{var r=e.head||e.getElementsByTagName("head")[0],a=e.createElement("script");a.setAttribute("type","text/javascript"),a.setAttribute("src",t),r.appendChild(a)}catch(t){}};t.CollectId = "5a4bc166dfcbbab4aa7c78b2",r("https://s3.amazonaws.com/collectchat/launcher.js")}(window,document);
  }


  // This is overlaying the nav when you click on hamburger menu
  $("#nav-icon").click(function() {
    $(this).toggleClass("animate-icon"), $("#overlay").fadeToggle();
    $('header').toggleClass("noborder");
  });
  $("#overlay").click(function() {
    $("#nav-icon").removeClass("animate-icon"), $("#overlay").toggle()
  });


  // $('#modal_contact_send').on('click', function(e){
  //   e.preventDefault();
  //   console.log('clicked');
  // });

  $("#close-youtube").on("click", function(){
    $('#video_iframe iframe').attr('src', $('#video_iframe iframe').attr('src'));
  })



// IOS bug therefore redirecting to connect page for mobile as opposed to popup

  const mq = window.matchMedia( "(max-width: 1199px)" );

  //The matches property returns true or false depending on the query result, e.g.

  if (mq.matches) {
    // window width is less than 1199px
    $('.go_to_connect').on('click', function(e){
      e.preventDefault();
      // window.location.href = '/connect'; // this reloads the page, we can do it with turbolinks
      Turbolinks.visit('/connect');
    });
  }; 

  //You can also add an event listener which fires when a change is detected:

  // media query event handler
  if (matchMedia) {
    const mq = window.matchMedia("(max-width: 1199px)");
    mq.addListener(WidthChange);
    WidthChange(mq);
  }

  // media query change
  function WidthChange(mq) {
    // window width is less than 1199px
    if (mq.matches) {
      $('.go_to_connect').on('click', function(e){
        e.preventDefault();
        // window.location.href = '/connect'; // this reloads the page, we can do it with turbolinks
        Turbolinks.visit('/connect');
      })
    } 
  }

// end IOS bug






//------------------------------------------------------------------------------------------------------//
  if ($('body.home.index').length > 0) {

    // PARALLAX
    // basically if screen width is 769px or greater do parallax
    const mq = window.matchMedia( "(min-width: 1000px)" );
    if (mq.matches) {
      var winH = window.innerHeight;
      var scroll_top = "500"

      function parallax() {

        // var prlx_welcome_lyr_0 = document.getElementById('prlx_welcome_lyr_0');
        // var prlx_welcome_lyr_1 = document.getElementById('prlx_welcome_lyr_1');
        // var prlx_welcome_lyr_2 = document.getElementById('prlx_welcome_lyr_2');
        // var prlx_welcome_lyr_3 = document.getElementById('prlx_welcome_lyr_3');
        // var prlx_welcome_lyr_4 = document.getElementById('prlx_welcome_lyr_4');
        
        // var prlx_about_lyr_0 = document.getElementById('prlx_about_lyr_0');
        // var prlx_about_lyr_1 = document.getElementById('prlx_about_lyr_1');
        // var prlx_about_lyr_2 = document.getElementById('prlx_about_lyr_2');
        // var prlx_about_lyr_3 = document.getElementById('prlx_about_lyr_3');

        // var prlx_spot_lyr_0 = document.getElementById('prlx_spot_lyr_0');
        // var prlx_spot_lyr_1 = document.getElementById('prlx_spot_lyr_1');
        // var prlx_spot_lyr_2 = document.getElementById('prlx_spot_lyr_2');
        // var prlx_spot_lyr_3 = document.getElementById('prlx_spot_lyr_3');
        
        // var portfolio = document.getElementById('portfolio');

        // var prlx_connect_lyr_0 = document.getElementById('prlx_connect_lyr_0');
        // var prlx_connect_lyr_1 = document.getElementById('prlx_connect_lyr_1');
        // var prlx_connect_lyr_2 = document.getElementById('prlx_connect_lyr_2');
        // var prlx_connect_lyr_3 = document.getElementById('prlx_connect_lyr_3');





        // All browsers now set id's as global variables, so no need for the above variable assignments,

        // negative values for window.pageYOffset makes the images go up when you scroll down
        // The smaller the number, the faster it moves

        //--------------------------------Welcome---------------------------------//

        prlx_welcome_lyr_0.style.top = (window.pageYOffset / 2)+'px'; //W
        prlx_welcome_lyr_1.style.top = (window.pageYOffset / 3)+'px'; //elcome text
        prlx_welcome_lyr_2.style.top = -(window.pageYOffset / 5) + 'px'; //frame
        prlx_welcome_lyr_3.style.top = -(window.pageYOffset / 1) + 'px'; //owl & cloud


        //--------------------------------About---------------------------------//

        // prlx_about_lyr_0.style.top = -(window.pageYOffset / 1) + winH +'px'; //A
        prlx_about_lyr_1.style.top = -(window.pageYOffset / 1) + winH + 'px'; //bout-text
        // prlx_about_lyr_2.style.top = -(window.pageYOffset / 1)  + winH + 'px'; //frame
        prlx_about_lyr_3.style.top = -(window.pageYOffset / 1) + winH + 'px'; // background-text


        // this stop the parallax once it reaches winH, i used this to reverse the direction
          if (window.pageYOffset > winH && $('body.home.index').length > 0) {
            prlx_about_lyr_1.style.top = 0 + 'px'; 
            
            function reverse_direction() {
              if ($('body.home.index').length > 0) {
                prlx_about_lyr_1.style.top = (window.pageYOffset / 4) - 0.25*winH + 'px';
                prlx_about_lyr_2.style.top = -(window.pageYOffset / 4) + 0.25*winH + 'px';
              }
            }
            
            window.addEventListener("scroll", reverse_direction, false);
          }


        //--------------------------------SPOTLIGHT---------------------------------//

        // prlx_spot_lyr_0.style.top = -(window.pageYOffset / 1) + 'px'; //S
        prlx_spot_lyr_1.style.top = -(window.pageYOffset / 5) + 0.2*2*winH +'px'; //potlight-text
        prlx_spot_lyr_2.style.top = (window.pageYOffset / 4) -0.25*2*winH + 'px'; //frame
        prlx_spot_lyr_3.style.top = -(window.pageYOffset / 1) + 2*winH +'px'; //Light



        //--------------------------------CONNECT---------------------------------//

        // prlx_connect_lyr_0.style.top = -(window.pageYOffset / .5) + 3*winH*2 + 'px'; //C
        prlx_connect_lyr_1.style.top = (window.pageYOffset / 4) -0.25*3*winH + 'px'; //onnect-text
        prlx_connect_lyr_2.style.top = -(window.pageYOffset / 5) + 0.2*3*winH + 'px'; //frame
        prlx_connect_lyr_3.style.top = (window.pageYOffset / 3.5) -(1/3.5)*3*winH + 'px'; //dots

      } // end parallax function

      $(window).scroll(function(){
        if ($('body.home.index').length > 0) {
          parallax();
        }
      });
    }

  } // home.index








//------------------------------------------------------------------------------------------------------//
  if ($('body.home.about').length > 0) {

    $('.owl-carousel').owlCarousel({
      loop:true,
      margin:10,
      nav:false,
      dots: false,
      autoplay: 3000,
      animateOut: 'fadeOut',
      responsiveClass:true,
      responsive:{
          0:{
              items:1
          }
      }
    });

   
    $('#creative_img').on({
      mouseenter: function() {
        $('#img_content').html('<p>We don’t simply design logos, create ads or build websites, we dig deeper. We’re talking sprawled on your therapist’s couch deep. Our team uncovers your brand’s story, then we articulate it with exceptional marketing pieces.</p>')
      },
      mouseleave: function() {
        $('#img_content').html('')
      }
    });

    $('#digital_img').on({
      mouseenter: function() {
        $('#img_content').html('<p>Call us nerds or call us passionate, but nothing excites us quite like SEO, social analytics and flawless code. When the time comes to take your brand to the digital sphere (hint: it probably already has), you can count on us to make sure you stand out.</p>')
      },
      mouseleave: function() {
        $('#img_content').html('')
      }
    });
   
    $('#marketing_img').on({
      mouseenter: function() {
        $('#img_content').html('<p>We brew up effective strategies just as frequently as we brew our coffee — and trust us, it’s around the clock. With an innovative and customized approach, we help you achieve your most ambitious, and even nearly impossible goals.</p>')
      },
      mouseleave: function() {
        $('#img_content').html('')
      }
    });
   
    $('#public_relations_img').on({
      mouseenter: function() {
        $('#img_content').html('<p>Your brand’s story deserves to be shouted from the rooftops, and we’re not afraid of heights. From securing media coverage to crafting social media content, we’ll make sure your audience sees you front and center.</p>')
      },
      mouseleave: function() {
        $('#img_content').html('')
      }
    });



    const mq2 = window.matchMedia( "(max-width: 480px)" );

    //The matches property returns true or false depending on the query result, e.g.

    if (mq2.matches) {
      // Instafeed
      var feed = new Instafeed({
        get: 'user',
        userId: '615590504',
        accessToken: '615590504.e19fde1.dd2936b0fd9145e0a78108e919903f0f',
        limit: 4,
        template: '<a class="imgWrap {{model.tags[0]}}" href="{{link}}" target="blank"><img src="{{image}}" /><span class="text"><i class="fa fa-heart-o" aria-hidden="true"></i> {{model.likes.count}} <i class="fa fa-comment-o ml20" aria-hidden="true"></i></i> {{model.comments.count}}</span></a>',
      });
      feed.run();
    } else {
      // Instafeed
      var feed = new Instafeed({
        get: 'user',
        userId: '615590504',
        accessToken: '615590504.e19fde1.dd2936b0fd9145e0a78108e919903f0f',
        limit: 9,
        template: '<a class="imgWrap {{model.tags[0]}}" href="{{link}}" target="blank"><img src="{{image}}" /><span class="text"><i class="fa fa-heart-o" aria-hidden="true"></i> {{model.likes.count}} <i class="fa fa-comment-o ml20" aria-hidden="true"></i></i> {{model.comments.count}}</span></a>',
      });
      feed.run();
    }; 

  } // home.about











//------------------------------------------------------------------------------------------------------//
  if ($('body.portfolio').length > 0) {

    $('.owl-carousel').owlCarousel({
      loop:true,
      margin:10,
      nav:false,
      dots: false,
      autoplay: 3000,
      // animateOut: 'fadeOut',
      responsiveClass:true,
      responsive:{
          0:{
              items:1
          },
          767:{
            items: 3
          }
      }
    });
  };











//------------------------------------------------------------------------------------------------------//

  if ($('body.blogs.index,body.blogs.index_category').length > 0) {
    var lp = $('#myId').data('last-page');
    if ($('.current').text().indexOf('Page') < 0) {
      $('.current').prepend('Page ').append(' of ').append(lp);
    }
    
    $('.previous_page').html('<span class="blog_page_arrow"><</span>');
    $('.next_page').html('<span class="blog_page_arrow">></span>');

    $last_page = $(".pagination a:nth-last-child(2)");
    var last_page_href = $last_page.attr('href');
    if ((last_page_href) && $('.last_page').text().indexOf('last') < 0) {
      $('.pagination').append('<a  class="last_page" href="' + last_page_href + '">last</a>');
    }
    
    $first_page = $(".pagination a:nth-child(2)");
    if ($first_page.text() == 1) {
      var first_page_href = $first_page.attr('href');
      $('.pagination').prepend('<a class="first_page" href="' + first_page_href + '">first</a>');
    }
  }




}); // turbolinks:load






//------------------------------------------------------------------------------------------------------//


//on turbolinks:before-cache destroy it owlCarousel
$(document).on('turbolinks:before-cache', function() {
  $('.owl-carousel').owlCarousel('destroy');
});

// This is adding the class circle-menu to the body when you scroll more than 400px from the top.
$(window).scroll(function() {    
    var scroll = $(window).scrollTop();

    if (scroll >= 400) {
        $("body").addClass("circle-menu");
    } else {
        $("body").removeClass("circle-menu");
    }
});



























// Maybe for instafeed

// if (window.matchMedia("(min-width: 400px)").matches) {
//      the viewport is at least 400 pixels wide 
// } else {
//     /* the viewport is less than 400 pixels wide */
// }

// Another example

// var mq = window.matchMedia('@media all and (max-width: 700px)');
// if(mq.matches) {
//     // the width of browser is more then 700px
// } else {
//     // the width of browser is less then 700px
// }
// Not only that, we can add an event listener and wait for changes.

// mq.addListener(function(changed) {
//     if(changed.matches) {
//         // the width of browser is more then 700px
//     } else {
//         // the width of browser is less then 700px
//     }
// });








// // Smooth scrolling to ID
// $(document).on('turbolinks:load', function() {
//   // Add smooth scrolling to all links
//   $("a").on('click', function(event) {

//     // Make sure this.hash has a value before overriding default behavior
//     if (this.hash !== "") {
//       // Prevent default anchor click behavior
//       event.preventDefault();

//       // Store hash
//       var hash = this.hash;

//       $('html, body').animate({
//         scrollTop: $(hash).offset().top
//       }, 800, function(){
   
//         // Add hash (#) to URL when done scrolling (default click behavior)
//         window.location.hash = hash;
//       });
//     } // End if
//   });
// });



// This is overlaying the nav when you click on hamburger menu
// $(document).on('turbolinks:load', function() {
//     $("#nav-icon").click(function() {
//         $(this).toggleClass("animate-icon"), $("#overlay").fadeToggle();
//         $('header').toggleClass("noborder");
//     });
//     $("#overlay").click(function() {
//         $("#nav-icon").removeClass("animate-icon"), $("#overlay").toggle()
//     });
// });



// Parallax
// $(document).on('turbolinks:load', function() {
//   // if ($('body.home.index').length > 0) {
//   //   // basically if screen width is 769px or greater do parallax
//   //   const mq = window.matchMedia( "(min-width: 769px)" );
//   //   if (mq.matches) {
//   //     var winH = window.innerHeight;
//   //     var scroll_top = "500"

//   //     function parallax() {

//   //       // var prlx_welcome_lyr_0 = document.getElementById('prlx_welcome_lyr_0');
//   //       // var prlx_welcome_lyr_1 = document.getElementById('prlx_welcome_lyr_1');
//   //       // var prlx_welcome_lyr_2 = document.getElementById('prlx_welcome_lyr_2');
//   //       // var prlx_welcome_lyr_3 = document.getElementById('prlx_welcome_lyr_3');
//   //       // var prlx_welcome_lyr_4 = document.getElementById('prlx_welcome_lyr_4');
        
//   //       // var prlx_about_lyr_0 = document.getElementById('prlx_about_lyr_0');
//   //       // var prlx_about_lyr_1 = document.getElementById('prlx_about_lyr_1');
//   //       // var prlx_about_lyr_2 = document.getElementById('prlx_about_lyr_2');
//   //       // var prlx_about_lyr_3 = document.getElementById('prlx_about_lyr_3');

//   //       // var prlx_spot_lyr_0 = document.getElementById('prlx_spot_lyr_0');
//   //       // var prlx_spot_lyr_1 = document.getElementById('prlx_spot_lyr_1');
//   //       // var prlx_spot_lyr_2 = document.getElementById('prlx_spot_lyr_2');
//   //       // var prlx_spot_lyr_3 = document.getElementById('prlx_spot_lyr_3');
        
//   //       // var portfolio = document.getElementById('portfolio');

//   //       // var prlx_connect_lyr_0 = document.getElementById('prlx_connect_lyr_0');
//   //       // var prlx_connect_lyr_1 = document.getElementById('prlx_connect_lyr_1');
//   //       // var prlx_connect_lyr_2 = document.getElementById('prlx_connect_lyr_2');
//   //       // var prlx_connect_lyr_3 = document.getElementById('prlx_connect_lyr_3');



//   //       // All browsers now set id's as global variables, so no need for the above variable assignments,


//   //       //--------------------------------Welcome---------------------------------//

//   //       // prlx_welcome_lyr_0.style.top = (window.pageYOffset / 0.2)+'px'; //W
//   //       // prlx_welcome_lyr_1.style.top = (window.pageYOffset / 0.2)+'px'; //elcome text
//   //       // prlx_welcome_lyr_2.style.top = -(window.pageYOffset / 0.2) + 'px'; //frame
//   //       // prlx_welcome_lyr_3.style.top = -(window.pageYOffset / 0.2) + 'px'; //owl
//   //       // prlx_welcome_lyr_4.style.top = -(window.pageYOffset / 0.1) + 'px'; //cloud


//   //       //--------------------------------About---------------------------------//

//   //       // prlx_about_lyr_0.style.top = -(window.pageYOffset / 1) + winH +'px'; //A
//   //       // prlx_about_lyr_1.style.top = -(window.pageYOffset / 1) + winH + 'px'; //bout-text
//   //       // prlx_about_lyr_2.style.top = -(window.pageYOffset / 1)  + 'px'; //frame
//   //       // prlx_about_lyr_3.style.top = -(window.pageYOffset / 1) + winH + 'px'; // background-text


//   //       // this stop the parallax once it reaches winH, i used this to reverse the direction
//   //         // if ($(this).scrollTop() > winH && $('body.home.index').length > 0) {
//   //         //   prlx_about_lyr_1.style.top = 0 + 'px'; 
            
//   //         //   function dmbig() {
//   //         //     if ($('body.home.index').length > 0) {
//   //         //       prlx_about_lyr_1.style.top = (window.pageYOffset / 4) - 0.25*winH + 'px';
//   //         //       prlx_about_lyr_2.style.top = -(window.pageYOffset / 4) + 0.25*winH + 'px';
//   //         //     }
//   //         //   }
            
//   //         //   window.addEventListener("scroll", dmbig, false);
//   //         // }


//   //       //--------------------------------SPOTLIGHT---------------------------------//

//   //       // prlx_spot_lyr_0.style.top = -(window.pageYOffset / 1) + 'px'; //S
//   //       // prlx_spot_lyr_1.style.top = -(window.pageYOffset / 3) + 'px'; //potlight-text
//   //       // prlx_spot_lyr_2.style.top = -(window.pageYOffset / 1) + 2*winH + 'px'; //frame
//   //       // prlx_spot_lyr_3.style.top = -(window.pageYOffset / 1) +  2*winH +'px'; //Light


//   //       //--------------------------------CASES---------------------------------//

//   //       // portfolio.style.top = -(window.pageYOffset / 1) + 'px';


//   //       //--------------------------------CONNECT---------------------------------//

//   //       // prlx_connect_lyr_0.style.top = -(window.pageYOffset / 1) + 'px'; //C
//   //       // prlx_connect_lyr_1.style.top = (window.pageYOffset / 1) + 'px'; //onnect-text
//   //       // prlx_connect_lyr_2.style.top = -(window.pageYOffset / 1) + 'px'; //frame
//   //       // prlx_connect_lyr_3.style.top = -(window.pageYOffset / 1) +  4.2*winH + 'px'; //owl
//   //       // prlx_connect_lyr_4.style.top = -(window.pageYOffset / 1) +  4.2*winH + 'px'; //clouds

//   //     } // end parallax function

//   //     $(window).scroll(function(){
//   //       if ($('body.home.index').length > 0) {
//   //         parallax();
//   //       }
//   //     });
//   //   }
//   // }
  
// });



















// var aspect_ratio = $(window).width()/$(window).height();
// console.log(aspect_ratio);
//   if (aspect_ratio < 2.5) {
//     $('.connect_button').css("top", 32.5*aspect_ratio + 16 + "%")
//   }
//   if (aspect_ratio < 1.25) {
//     $('.connect_button').css("top", 38.6*aspect_ratio + 60 + "%")
//   }
//   if (aspect_ratio < 1) {
//     $('.connect_button').css("top", 38.6*aspect_ratio + 60 + "%")
//   }

// This is using aspect ration to keep the connect button position correct.
// $( window ).resize(function() {
//   var aspect_ratio = $(window).width()/$(window).height();
//   console.log(aspect_ratio);
//   if (aspect_ratio < 2.5) {
//     $('.connect_button').css("top", 32.5*aspect_ratio + 16 + "%")
//   }
//   if (aspect_ratio < 1) {
//     $('.connect_button').css("top", 38.6*aspect_ratio + 60 + "%")
//   }
// })
