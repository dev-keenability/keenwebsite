// Parallax 1
// $(document).on('turbolinks:load', function() {

//   $('.aa-paralax').each(function(){
//     var $bgobj = $(this);
//     $(window).scroll(function() {

//       var offset = $bgobj.offset();
//       var elemTop = offset.top;
//       var windowBottom = $(window).scrollTop() + $(window).height();

//        // Calculate offset based on element relative to window
//        var elemRel = elemTop - $(window).scrollTop();
//        var yPos = elemRel / $bgobj.data('speed');
//        var coords = '50% '+ yPos + 'px';
       
//        if((elemRel < 0 || elemRel < $(window).height()) && elemRel > -$bgobj.height()){
//          $bgobj.css({ backgroundPosition: coords });       
//       }

//     });
//   }); 
// });






// Parallax 2
// function parallax() {
//   var prlx_lyr_1 = $('#prlx_lyr_1');
//   var prlx_lyr_2 = $('#prlx_lyr_2');
//   prlx_lyr_1.style.top = -(window.pageYOffset / 4) +'px';
//   prlx_lyr_2.style.top = -(window.pageYOffset / 10) +'px';
// }  
// $(window).addEventListener("scroll", parallax, false);




// This is adding the class circle-menu to the body when you scroll more than 200px from the top.
// $(window).scroll(function() {    
//     var scroll = $(window).scrollTop();

//     if (scroll >= 200) {
//         $("body").addClass("circle-menu");
//     } else {
//         $("body").removeClass("circle-menu");
//     }
// });


// Adding classes for animation upon scroll
// $(window).scroll(function() {
//   $('#title-work').each(function(){
//   var imagePos = $(this).offset().top;
  
//   var topOfWindow = $(window).scrollTop();
//     if (imagePos < topOfWindow+700) {
//       $(this).addClass("animated slideInDown myvisible");
//     }
//   });

//   $('#btn-work').each(function(){
//   var imagePos = $(this).offset().top;
  
//   var topOfWindow = $(window).scrollTop();
//     if (imagePos < topOfWindow+1000) {
//       $(this).addClass("animated zoomIn myvisible");
//     }
//   });

//   $('#title-about').each(function(){
//   var imagePos = $(this).offset().top;
  
//   var topOfWindow = $(window).scrollTop();
//     if (imagePos < topOfWindow+700) {
//       $(this).addClass("animated slideInRight myvisible");
//     }
//   });

//   $('#btn-about').each(function(){
//   var imagePos = $(this).offset().top;
  
//   var topOfWindow = $(window).scrollTop();
//     if (imagePos < topOfWindow+1000) {
//       $(this).addClass("animated slideInLeft myvisible");
//     }
//   });  

//   $('#title-contact').each(function(){
//   var imagePos = $(this).offset().top;
  
//   var topOfWindow = $(window).scrollTop();
//     if (imagePos < topOfWindow+700) {
//       $(this).addClass("animated zoomIn myvisible");
//     }
//   });

//   $('#btn-contact').each(function(){
//   var imagePos = $(this).offset().top;
  
//   var topOfWindow = $(window).scrollTop();
//     if (imagePos < topOfWindow+1000) {
//       $(this).addClass("animated flash myvisible");
//     }
//   });          
// });


// $( document ).ready(function() {
//  var theToggle = document.getElementById('toggle');

//   // based on Todd Motto functions
//   // https://toddmotto.com/labs/reusable-js/

//   // hasClass
//   function hasClass(elem, className) {
//    return new RegExp(' ' + className + ' ').test(' ' + elem.className + ' ');
//   }
//   // addClass
//   function addClass(elem, className) {
//       if (!hasClass(elem, className)) {
//        elem.className += ' ' + className;
//       }
//   }
//   // removeClass
//   function removeClass(elem, className) {
//    var newClass = ' ' + elem.className.replace( /[\t\r\n]/g, ' ') + ' ';
//    if (hasClass(elem, className)) {
//           while (newClass.indexOf(' ' + className + ' ') >= 0 ) {
//               newClass = newClass.replace(' ' + className + ' ', ' ');
//           }
//           elem.className = newClass.replace(/^\s+|\s+$/g, '');
//       }
//   }
//   // toggleClass
//   function toggleClass(elem, className) {
//    var newClass = ' ' + elem.className.replace( /[\t\r\n]/g, " " ) + ' ';
//       if (hasClass(elem, className)) {
//           while (newClass.indexOf(" " + className + " ") >= 0 ) {
//               newClass = newClass.replace( " " + className + " " , " " );
//           }
//           elem.className = newClass.replace(/^\s+|\s+$/g, '');
//       } else {
//           elem.className += ' ' + className;
//       }
//   }

//   theToggle.onclick = function() {
//      toggleClass(this, 'on');
//      return false;
//   }
// });