module ApplicationHelper
    def is_category_active?(category_slug)
       current_page?(controller: 'blogs', action: 'index_category', category: category_slug) ? "cat_active" : ""
    end
    def is_active?(category_id)
       current_page?(controller: 'blogs', action: 'show') ? "cat_active" : ""
    end
end
