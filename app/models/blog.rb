class Blog < ApplicationRecord
  belongs_to :category

  validates :title, uniqueness: {case_sensitive: false}

  before_create :title_url_it
  before_save :remove_img
  # after_commit :set_imgs, on: [:create, :update]

  has_many :images, as: :imageable, :class_name => "Blog::Asset", dependent: :destroy
  accepts_nested_attributes_for :images, allow_destroy: true
  
  #This is for SEO purposes, you want the url to be the title of the blog instead of its ID
  def title_url_it
    self.title_for_slug = title.downcase.squish.parameterize("-")
  end

  def remove_img
    nokogiri_html_document =  Nokogiri::HTML(self.content) #heroku does not like tmp directory
    nokogiri_html_document.search('img').each do |img|
      img.remove
    end
    nokogiri_html_document.search('a').each { |node| node.replace(node.text) }
    self.content_index = nokogiri_html_document
    #note that if you call self.save here, you might run into infinite loops (stack level too deep), even for after_save, after_create and after_update callbacks
  end

  # def set_imgs
  #   self.update_attributes(image_medium: self.images[0].pic.url(:medium)) if self.images.present?
  #   self.update_attributes(image_large: self.images[0].pic.url(:large)) if self.images.present?
  #   self.image_medium = self.images[0].pic.url(:medium) if self.images.present?
  #   self.image_large = self.images[0].pic.url(:large) if self.images.present?
  # end

end
