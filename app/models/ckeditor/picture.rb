class Ckeditor::Picture < Ckeditor::Asset
  has_attached_file :data,
                    :url => "#{Rails.env}/ckeditor/:id/:style/:filename",
                    :path => "#{Rails.env}/ckeditor/:id/:style/:filename",
                    :styles => { 
                                 :medium => ["300x190>", :jpg], 
                                 :large => ["600x380>", :jpg]
                               }

  validates_attachment_presence :data
  validates_attachment_size :data, :less_than => 2.megabytes
  validates_attachment_content_type :data, :content_type => /\Aimage/

  def url_content
    url(:large)
  end
end
