class BlogsController < ApplicationController
  before_action :set_blog, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:new, :create, :edit, :update, :destroy]
  def index
    @blogs = Blog.order(pubdate: :desc).page(params[:page]).per_page(6)
    @categories = Category.order(name: :asc)
  end

  def index_category
    @blogs = Blog.where(category_id: Category.find_by(category_slug: params[:category]).id).order(pubdate: :desc).page(params[:page]).per_page(6)
    @categories = Category.order(name: :asc)
    render :index
  end

  def show
    # this is because our blog urls start from root path and when someone tries to route to 
    # /esh it was trying to find a blog with title esh and giving error because set blog function was returning nil.
    if !@blog.present?
      render 'home/catch_all'
      # return redirect_to :controller => 'home', :action => 'catch_all'
    else
      @related_blogs = Blog.where(category_id: @blog.category_id).where.not(id: @blog.id).last(3)
      @categories = Category.order(name: :asc)
    end
  end

  def new
    @blog = Blog.new
  end

  def edit
  end

  def create
    @blog = Blog.new(blog_params)
    if @blog.save
      set_imgs
      redirect_to blog_path(@blog.title_for_slug), notice: 'Blog was successfully created.'
    else
      render :new
    end
  end

  def update
    if @blog.update(blog_params)
      set_imgs
      redirect_to blog_path(@blog.title_for_slug), notice: 'Blog was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @blog.destroy
    redirect_to blogs_url, notice: 'Blog was successfully destroyed.'
  end

  private

    def set_blog
      @blog = Blog.find_by(title_for_slug: params[:title_for_slug])
    end

    def set_imgs
      @blog.image_medium = @blog.images.order(updated_at: :desc).first.pic.url(:medium) if @blog.images.present?
      @blog.image_large = @blog.images.order(updated_at: :desc).first.pic.url(:large) if @blog.images.present?
      @blog.save
    end

    def blog_params
      params.require(:blog).permit(:title, :content, :content_index, :title_for_slug, :pubdate, :category_id, :meta_description, :meta_keywords, images_attributes: [:id, :pic, :name, :_destroy])
    end

end
