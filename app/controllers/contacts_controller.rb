class ContactsController < ApplicationController
  def create
    @contact = Contact.new(contact_params)

    respond_to do |format|
      if @contact.save
        format.html {
          ContactMailer.welcome_email(@contact).deliver_later
          session[:thank_you] = "Thank you"
          redirect_to connect_path(anchor: 'cform')
        }
        format.js {
          ContactMailer.welcome_email(@contact).deliver_later
        }
      end
    end
  end

  private

  def contact_params
    params.require(:contact).permit(:name, :email, :phone_number, :message)
  end
end
