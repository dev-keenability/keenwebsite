class HomeController < ApplicationController
  def index
  end

  def about
  end

  def connect
    @thank_you = session[:thank_you]
    session[:thank_you] = nil
  end

  def catch_all
  end
end
