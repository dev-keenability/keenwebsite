class ContactMailer < ApplicationMailer
  default from: ENV["GMAIL_USERNAME"]

  def welcome_email(contact)
    @contact = contact
    mail(to: "hello@keenability.com", subject: 'You have received a new lead for Keenability')
  end
end
