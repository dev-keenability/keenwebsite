class RemoveMainImageAddImageMediumLargeToBlogs < ActiveRecord::Migration[5.0]
  def change
    remove_column :blogs, :main_image, :string
    add_column :blogs, :image_medium, :string
    add_column :blogs, :image_large, :string
    add_column :blogs, :category_id, :integer
  end
end
