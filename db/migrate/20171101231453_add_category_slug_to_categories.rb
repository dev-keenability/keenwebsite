class AddCategorySlugToCategories < ActiveRecord::Migration[5.0]
  def up
    add_column :categories, :category_slug, :string
    Category.find_each do |category|
      category.category_slug = "#{category.name}".downcase.squish.parameterize("-")
      category.save!
    end
  end

  def down
    remove_column :categories, :category_slug
  end
end

