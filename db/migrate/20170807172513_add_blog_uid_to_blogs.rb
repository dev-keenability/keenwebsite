class AddBlogUidToBlogs < ActiveRecord::Migration[5.0]
  def change
    add_column :blogs, :blog_uid, :integer
  end
end
