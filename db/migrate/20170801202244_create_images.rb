class CreateImages < ActiveRecord::Migration[5.0]
  def change
    create_table :images do |t|
      t.references :imageable, polymorphic: true
      t.string :name
      t.attachment :pic
      t.boolean :primary

      t.timestamps
    end
  end
end
