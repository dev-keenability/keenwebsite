Rails.application.routes.draw do
  require "admin_constraint"
  mount Ckeditor::Engine => "/ckeditor", constraints: AdminConstraint.new

  devise_scope :user do
    get "/users/sign_up",  :to => "devise/sessions#new"
  end
  
  devise_for :users

  devise_scope :user do 
    get "/admin" => "devise/sessions#new" 
  end
  
  root 'home#index'

  match '/about'                                      => 'home#about',                               via: [:get],            :as      => 'about'
  match '/connect'                                    => 'home#connect',                             via: [:get],            :as      => 'connect'
  match '/privacy-policy'                             => 'home#privacy_policy',                      via: [:get],            :as      => 'privacy_policy'
  
  get '/contact', to: redirect('/connect')
  get '/contact-us', to: redirect('/connect')
  get '/lets-meet', to: redirect('/connect')
  get '/our-work', to: redirect('/portfolio')
  get '/insights', to: redirect('/blogs')
  get '/about-us', to: redirect('/about')
  get '/services', to: redirect('/about')

  match '/portfolio'                                  => 'portfolio#index',                          via: [:get],            :as      => 'portfolio'
  match '/portfolio/boca-luxury-realty'               => 'portfolio#boca_luxury_realty',             via: [:get],            :as      => 'boca_luxury_realty'
  match '/portfolio/gcg-group-rebrand'                => 'portfolio#gcg_group_rebrand',              via: [:get],            :as      => 'gcg_group_rebrand'
  match '/portfolio/old-palm-golf-club'                    => 'portfolio#oldpalm',                  via: [:get],            :as      => 'oldpalm'
  match '/portfolio/fau'                              => 'portfolio#fau',                            via: [:get],            :as      => 'fau'
  match '/portfolio/graham-legal'                     => 'portfolio#graham_legal',                   via: [:get],            :as      => 'graham_legal'
  match '/portfolio/stein-posner'                     => 'portfolio#stein_posner',                   via: [:get],            :as      => 'stein_posner'

  match "/contacts"              =>        "contacts#create",              via: [:post]

  get '/sitemap.xml', to: redirect("https://s3.amazonaws.com/keenwebsite/sitemaps/sitemap.xml.gz", status: 301)

  #########################################
  #==news url for blog model
  #########################################
    get "/blogs", to: "blogs#index", as: :blogs
    post "blogs", to: "blogs#create"
    get "/blogs/new", to: "blogs#new", as: :new_blog
    get "/blogs/:category", to: "blogs#index_category", as: :blogs_index_category
    get "/:title_for_slug/edit", to: "blogs#edit", as: "edit_blog"
    get "/:title_for_slug", to: "blogs#show", as: "blog"
    patch "/:title_for_slug", to: "blogs#update"
    put "/:title_for_slug", to: "blogs#update"
    delete "/:title_for_slug", to: "blogs#destroy"


  match "*path", to: "home#catch_all", via: :all
  
end 
 