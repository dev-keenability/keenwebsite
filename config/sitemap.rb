# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "https://www.keenability.com"

SitemapGenerator::Sitemap.adapter = SitemapGenerator::AwsSdkAdapter.new(
  "keenwebsite",
  aws_access_key_id: ENV["S3_ACCESS_KEY_ID"],
  aws_secret_access_key: ENV["S3_SECRET_ACCESS_KEY"],
  aws_region: 'us-east-1'
)

SitemapGenerator::Sitemap.sitemaps_host = "https://s3.amazonaws.com/keenwebsite/"
SitemapGenerator::Sitemap.sitemaps_path = 'sitemaps/'

SitemapGenerator::Sitemap.create do

  add blogs_path, :priority => 1.0, :changefreq => 'daily'
  Category.find_each do |category|
    add blogs_index_category_path(category.category_slug), :priority => 0.8, :changefreq => 'daily'
  end
  Blog.find_each do |blog|
    add blog_path(blog.title_for_slug), :changefreq => 'daily', :lastmod => blog.updated_at, :priority => 1.0
  end

  add about_path, :priority => 0.8, :changefreq => 'monthly'

  add portfolio_path, :priority => 0.8, :changefreq => 'monthly'
  add boca_luxury_realty_path, :priority => 0.8, :changefreq => 'monthly'
  add gcg_group_rebrand_path, :priority => 0.8, :changefreq => 'monthly'
  add oldpalm_path, :priority => 0.8, :changefreq => 'monthly'
  add fau_path, :priority => 0.8, :changefreq => 'monthly'
  add graham_legal_path, :priority => 0.8, :changefreq => 'monthly'
  add stein_posner_path, :priority => 0.8, :changefreq => 'monthly'

  add connect_path, :priority => 0.7, :changefreq => 'monthly'


end
