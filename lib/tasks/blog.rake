namespace :blog do 
  task :pullit => :environment do
    
    begin
      AlexLogger.info "hello world"
      nokogiri_xml_document =  Nokogiri::XML(File.open("#{Rails.root}/tmp/keenability-wordpress-2017-07-28.xml")) #usually in tmp but git ignores tmp folder
      AlexLogger.info nokogiri_xml_document.class

      hash = Hash.from_xml(nokogiri_xml_document.to_s)
      @items = hash["rss"]["channel"]["item"]
      AlexLogger.info @items.first
      AlexLogger.info @items.count

      @items.each do |item|

        # # Create Categories
        # if item["category"].is_a?(Array)
        #   item["category"].each do |category|
        #     Category.where(name: category.capitalize.squish).first_or_create do |ctg|
        #       ctg.name = category
        #     end
        #   end
        # else
        #   Category.where(name: item["category"].capitalize.squish).first_or_create do |ctg|
        #     ctg.name = item["category"]
        #   end
        # end

        # #Creating BlogsCategories
        # if item["category"].is_a?(Array)
        #   item["category"].each do |category|
        #     my_blog.categories << Category.where(name: category).first
        #   end
        # else
        #   my_blog.categories << Category.where(name: item["category"]).first
        # end

        # Create Blogs
        my_blog = Blog.new
        my_blog.title = item["title"]
        my_blog.content = item["encoded"][0]
        my_blog.pubdate = item["post_date"]
        my_blog.blog_uid = item["post_id"]
        my_blog.title_for_slug = item["link"].split('/').last
        item["postmeta"].each do |meta_hash|
          if meta_hash["meta_key"] == "_yoast_wpseo_metadesc"
             my_blog.meta_description = meta_hash["meta_value"] 
          elsif meta_hash["meta_key"] == "qode_seo_description"
            my_blog.meta_description = meta_hash["meta_value"] 
          end
        end
        my_blog.category = Category.first
        
        my_blog.save!

      end
      #@items.each





    rescue Exception => e
      AlexLogger.info e.message
      # AlexLogger.info e.backtrace  #this will fill up the logs pretty quickly...use only if necessary
    end #begin

  end #task















  ##=========================================================================================##
  ## Pulling images
  ##=========================================================================================##
  task :image_pullit => :environment do
    nokogiri_xml_image_document =  Nokogiri::XML(File.open("#{Rails.root}/tmp/keenability-wordpress-images-2017-07-28.xml")) #usually in tmp but git ignores tmp folder
    image_hash = Hash.from_xml(nokogiri_xml_image_document.to_s)
    @image_items = image_hash["rss"]["channel"]["item"]
    
    AlexLogger.info @image_items.first
    AlexLogger.info @image_items.count

    @image_items.each_with_index do |image_item, i|
        blog = Blog.where(blog_uid: image_item['post_parent'].to_i).first
        AlexLogger.info image_item['post_parent'].to_i
        if !blog.nil?
          my_img_dir = File.dirname("#{Rails.root}/tmp/keen-blog-images/#{blog.id}/#{image_item['attachment_url'].split('/').last}")
          FileUtils.mkdir_p(my_img_dir) unless File.directory?(my_img_dir)
          `cd #{Rails.root}/tmp/keen-blog-images/#{blog.id} && curl -o #{blog.title_for_slug}.#{image_item['attachment_url'].split('.').last} #{image_item["attachment_url"]}`
          # Upload to paperclip
          file = File.open("#{Rails.root}/tmp/keen-blog-images/#{blog.id}/#{blog.title_for_slug}.#{image_item['attachment_url'].split('.').last}")
          blog.images.create(pic: file)
          file.close
        end
    end

  end










  ##=========================================================================================##
  ## Set images
  ##=========================================================================================##
  task :set_imgs => :environment do
    blogs = Blog.all

    blogs.each do |blog|
      if blog.images.present?
        AlexLogger.info blog.images[0].pic.url(:medium).split('?').first
        blog.image_medium = blog.images[0].pic.url(:medium).split('?').first if blog.images.present?
        blog.image_large = blog.images[0].pic.url(:large).split('?').first if blog.images.present?
        blog.save
      end
    end

  end














  ##=========================================================================================##
  ## Just to clean up while practicing
  ##=========================================================================================##
  task :count_brackets => :environment do
    nokogiri_xml_document =  Nokogiri::XML(File.open("#{Rails.root}/tmp/keenability-wordpress-2017-07-28.xml")) #usually in tmp but git ignores tmp folder
      AlexLogger.info "Count Brackets"
      AlexLogger.info ""

      hash = Hash.from_xml(nokogiri_xml_document.to_s)
      @items = hash["rss"]["channel"]["item"]

      count = 0
      @items.each do |item|
        # if item["encoded"][0].include?("<a")
        #   count += 1
        #   AlexLogger.info item["title"]
        # end
        AlexLogger.info item["encoded"][0].class
      end

      AlexLogger.info count

  end
  #task:cleanup












  ##=========================================================================================##
  ## Check column uniqueness
  ##=========================================================================================##
  task :column_uniq => :environment do
    AlexLogger.info "hello world"
    nokogiri_xml_document =  Nokogiri::XML(File.open("#{Rails.root}/tmp/keenability-wordpress-2017-07-28.xml")) #usually in tmp but git ignores tmp folder
    AlexLogger.info nokogiri_xml_document.class

    hash = Hash.from_xml(nokogiri_xml_document.to_s)
    @items = hash["rss"]["channel"]["item"]
    AlexLogger.info @items.first
    AlexLogger.info @items.count

    @items.each do |item|
      AlexLogger.info item["post_id"].class
    end

    my_array =[]
    # Blog.all.each do |blog|
    #   my_array << blog.title_for_slug
    # end
    # AlexLogger.info my_array.count
    # AlexLogger.info my_array.uniq.count
    # AlexLogger.info my_array.detect{ |e| my_array.count(e) > 1 }
  end











  ##=========================================================================================##
  ## Just to clean up while practicing
  ##=========================================================================================##
  task :cleanup => :environment do
    Category.delete_all
    Blog.delete_all
    ActiveRecord::Base.connection.execute("DELETE from blogs_categories")
    File.truncate("#{Rails.root}/tmp/blog_content.txt", 0) #this removes all the contents in the file
    File.truncate("#{Rails.root}/tmp/blog_content.html", 0) #this removes all the contents in the file
  end
  #task:cleanup

end #namespace